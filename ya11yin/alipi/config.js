//A sample configuration file for JS variables.  Copy this file to "config.js"
var config = {
    'hostname': "y.a11y.in",
    'deploy': "http://y.a11y.in/web",
    'root': "http://y.a11y.in",
	'sweet': "http://teststore.swtr.us",
  'app_id': 'px3XaeXii8p273ouod2rwIKY7yk3psHVBMenoTM9',
  'app_secret': 'eQTURQTrHObD29dmVhf7fGajB1tF5cMfJKurOtz3vEx26Snmt0',
  "endpoints": { "get": "/api/sweets/q",
                "post": "/api/sweets",
                "auth": "/oauth/authorize",
                "login": "/auth/login",
                "logout": "/auth/logout"
              },
  'oauth_redirect_uri': 'http://y.a11y.in/web/redirect'
};

